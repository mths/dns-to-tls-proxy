**Overview**

This python module (dnstlsproxy) is a simple implementation of a DNS to TLS proxy server that allows services to route their unencrypted DNS queries to an encrypted DNS server endpoint.

**Implementation**

Supports UDP and TCP connection.

Because of the multithreaded implementation, the proxy server allows incoming requests at the same time, even if its an UDP or TCP request.

**Security concerns**

The traffic between the client and the DNS Proxy is not encrypted, for that reason is highly recommended that the proxy server runs inside a DMZ protected zone and "closest" as possible to the client service to reduce the attack surface of a man in the middle attack.

For testing purposes, the proxy has an optional parameter that allows ignoring the certificate verification. This can become a security issue if the proxy server connects to an untrusted DNS server. In a production environment, this is a bad practice.

**Integrating to a distributed, microservices-oriented and containerized architecture**

I believe that in a production container environment like Kubernetes the best approach is to use the dnstlsproxy container as a sidecar to the service(dns client) container. This way you can configure the service container to DNS resolve to the sidecar dnstlsproxy container so the DNS queries go through the sidecar assuring that all communication (inbound and outbound) exiting the pod to the DNS server is encrypted.

It also makes harder to an attacker to sniff/spoof the communication between the service and the dnstlsproxy since the exposed surface is inside the POD.

**Future improvements**

* Support to environment variables as configuration parameters to facilitate the use on containers.
* Do a few tests with multiprocessing instead of multithreading because of the global interpreter lock when working with threads.
* Improve error messages and exception handling


**How to use - CLI Daemon**

The module requires Python 3+. If needed, configure a virtualenv on the project folder.

> `virtualenv venv -p python3`
> `source venv/bin/active`

Build/Install the pip module

> `python setup.py install`

Run the dnstlsproxy module with debug mode enabled

> `dnstlsproxy --debug_mode=True`

**How to use - Docker container**

Build a local docker image

> `docker build . -t dns-proxy-server:0.1.0`

Run the docker image and expose the port 53 of the container as 10053 on the host (You can expose port 53 if you want to) for TCP and UPD connections

> `docker run -tid -p 10053:53 -p 10053:53/udp dns-proxy-server:0.1.0`

**How to test the DNS queries to the DNS proxy server**

After the proxy server is running, run the following commands:

> `nslookup n26.com 127.0.0.1 -port=10053` (UDP connection)

> `dig @127.0.0.1 -p 10053 n26.com +tcp` (TCP connection)

**Additional Arguments**

You can pass the following arguments to the dnstlsproxy cli. Ex: dnstlsproxy --listen_port 123456

| argument | description | default |
| ------ | ------ | ------ |
| --listen_port | Port that the DNS proxy server will listen to | 0.0.0.0 |
| --listen_ip | Ip that the DNS proxy server will listen to | 53 |
| --cert_verify | Set to false to ignore certificate verification | True |
| --cert_ca_path |Path to the CA root or CA Bundle certificates for verification | /etc/ssl/certs/ca-certificates.crt |
| --dns_tls_server | DNS TLS server to proxy to | 1.1.1.1 |
| --dns_tls_server_port | DNS TLS server port to proxy to | 853 |
| --debug_mode | Set to true to enable debug mode | False |

