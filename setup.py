from setuptools import setup

setup(
    name='dnstlsproxy',
    version='0.1.0',
    author='Matheus Bessa',
    author_email='matheusbpf@gmail.com',
    description='Python DNS to TLS proxy',
    classifiers=["Development Status :: 3 - Alpha",
                 "Environment :: Console",
                 "Topic :: Utilities"],
    packages=['dnstlsproxy'],
    python_requires='>=3.2',
    entry_points={
        'console_scripts': [
            'dnstlsproxy = dnstlsproxy.dnstlsproxy:run',
        ],
    },
)
