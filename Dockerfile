FROM python:3-alpine

USER root

COPY ./ /dnstlsproxy

WORKDIR /dnstlsproxy

RUN python3 setup.py install

ENTRYPOINT [ "dnstlsproxy" ]